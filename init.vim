call plug#begin(stdpath('data') . '/init.vim')

" For LaTeX
Plug 'vim-latex/vim-latex'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-commentary' " Commentary.vim
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-eunuch'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes' " Airline Themes
Plug 'michaeljsmith/vim-indent-object'

" Plug 'jiangmiao/auto-pairs'
Plug 'machakann/vim-highlightedyank'
Plug 'junegunn/fzf.vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'ctrlpvim/ctrlp.vim'

" NerdTree
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'scrooloose/nerdtree'

" Plug 'Reewr/vim-monokai-phoenix'
" Plug 'rafi/awesome-vim-colorschemes'
" Plug 'terryma/vim-multiple-cursors'
Plug 'Guzzii/python-syntax'
Plug 'mileszs/ack.vim'
Plug 'gregsexton/MatchTag'
Plug 'xolox/vim-misc'
Plug 'xolox/vim-easytags'
Plug 'jceb/vim-orgmode'
" Plug 'joshdick/onedark.vim'
Plug 'gruvbox-community/gruvbox'
" Plug 'sainnhe/gruvbox-material'
" Plug 'tjdevries/colorbuddy.nvim'
" Plug 'npxbr/gruvbox.nvim'
Plug 'vim-scripts/ReplaceWithRegister'
Plug 'christoomey/vim-system-copy'
" Plug 'python-mode/python-mode', { 'branch': 'develop' }
" Plug 'tikhomirov/vim-glsl'
" Plug 'vim-scripts/clips.vim'
Plug 'justinmk/vim-syntax-extra'
" " Pending tasks list
" Plug 'fisadev/FixedTaskList.vim' ", { 'for': 'python' }
" " " Python and other languages code checker
" Plug 'scrooloose/syntastic' , { 'for': 'python' }
" " Indent text object
" Plug 'michaeljsmith/vim-indent-object' , { 'for': 'python' }
" " Indentation based movements
" Plug 'jeetsukumaran/vim-indentwise' , { 'for': 'python' }
" " Python autocompletion, go to definition.
" Plug 'davidhalter/jedi-vim', { 'for': 'python' }
" " Better autocompletion
" Plug 'Shougo/neocomplcache.vim' , { 'for': 'python' }

" " Haskell prettifiers
" Plug 'jaspervdj/stylish-haskell'
" Plug 'nbouscal/vim-stylish-haskell'
Plug 'sheerun/vim-polyglot'
" Plug 'chrisdone/hindent' , { 'for': 'haskell' }
Plug 'alx741/vim-hindent' , { 'for': 'haskell' }
" " Errors highlighting
Plug 'dense-analysis/ale' , { 'for': 'haskell' }
" " Syntax highlighting
Plug 'neovimhaskell/haskell-vim' , { 'for': 'haskell' }
" " noseke de que se actualiza solo haskell
" Plug 'bitc/vim-hdevtools'


" PYTHON
Plug 'sbdchd/neoformat'
Plug 'neomake/neomake'
Plug 'tmhedberg/SimpylFold'
" LSP
Plug 'neoclide/coc.nvim', {'branch': 'release'} "pynvim need to be installed
Plug 'pappasam/coc-jedi', { 'do': 'yarn install --frozen-lockfile && yarn build' }
" No LSP
" Plug 'davidhalter/jedi-vim'
" Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
" Plug 'zchee/deoplete-jedi'

" HTML + php
Plug 'captbaritone/better-indent-support-for-php-with-html'

" Vim Orgmode
Plug 'jceb/vim-orgmode'
Plug 'dhruvasagar/vim-table-mode'
Plug 'tpope/vim-speeddating'

" Emacs like plugins
Plug 'liuchengxu/vim-which-key'

" HaProxy
Plug 'sclo/haproxy.vim'

""""""""""""""""""""""""" Clap installation
" Clap (find-file)
Plug 'liuchengxu/vim-clap'

" Build the extra binary if cargo exists on your system.
" Plug 'liuchengxu/vim-clap', { 'do': ':Clap install-binary' }

" The bang version will try to download the prebuilt binary if cargo does not exist.
" Plug 'liuchengxu/vim-clap', { 'do': ':Clap install-binary!' }

" :Clap install-binary[!] will always try to compile the binary locally,
" if you do care about the disk used for the compilation, try using the force download way,
" which will download the prebuilt binary even you have installed cargo.
" Plug 'liuchengxu/vim-clap', { 'do': { -> clap#installer#force_download() } }

" `:Clap install-binary[!]` will run using the terminal feature which is inherently async.
" If you don't want that and hope to run the hook synchorously:
" Plug 'liuchengxu/vim-clap', { 'do': has('win32') ? 'cargo build --release' : 'make' }
"""""""""""""""""""""""

Plug 'vimwiki/vimwiki', { 'branch': 'dev' }
Plug 'tools-life/taskwiki'
" markdown:
" https://github.com/WnP/vimwiki_markdown : pip install vimwiki-markdown
"
" " Recommended optional dependencies for taskwiki
Plug 'powerman/vim-plugin-AnsiEsc'
Plug 'preservim/tagbar'
Plug 'blindFS/vim-taskwarrior'
Plug 'itchyny/calendar.vim'
" Plug 'mattn/calendar-vim'
" Calendar.vim modificado por un chino:
Plug 'ZhangDezhi/diary-vim'

call plug#end()

let g:airline_theme='violet'
let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => NERDTree
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Uncomment to autostart the NERDTree
" autocmd vimenter * NERDTree
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'
let NERDTreeShowLineNumbers=1
let NERDTreeShowHidden=1
let NERDTreeMinimalUI = 1

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
hi LineNr ctermfg=242
hi CursorLineNr ctermfg=15
hi VertSplit ctermfg=8 ctermbg=0
hi Statement ctermfg=3

" Settings

" LaTeX
let g:tex_flavor='latex'

" Numbers
:set number
:set relativenumber

" Color scheme + syntax
syntax on

let g:gruvbox_colors = {
			\ }
colorscheme gruvbox
" lua require("colorbuddy").colorscheme("gruvbox")

" CUSTOM CONFIG
" Search down in subforlders
set path+=**
set nocompatible
set encoding=utf-8
" syntax enable
filetype on
filetype plugin indent on
set autoindent
set laststatus=2
set incsearch
set hlsearch
set cursorline

" Didsplay all atvhing files when we tab complete
set wildmenu

" map <C-p> <C-]>

" TAGS
 " Create the 'tags' file (may need to install ctags first)
let g:autotagTagsFile=".tags"
 " JAVA
command! MakeTags !ctags -R .

" Recompile groff document on write
autocmd BufWritePost *.ms | :!groff -ms %:p -ep -T pdf > %:r.pdf

" Remove trailing whitespaces on any file
autocmd BufWritePre * :%s/\s\+$//e

 " C
" set termguicolors
set t_Co=256
nnoremap <SPACE> <Nop>
let g:mapleader = "\<Space>"
" Use the following on classic vim
" map <Space> <Leader>
let g:maplocalleader = "\<Space>"
nnoremap <silent> <leader> :WhichKey '<Space>'<CR>
nnoremap <silent> <localleader> :<c-u>WhichKey  '<Space>'<CR>

" Folds
set foldlevelstart=2
" set nofoldenable
" set foldmethod=indent
" set foldnestmax=10

" XML/HTML
imap <silent> <C-c> </<C-X><C-O><C-X><Esc>xa
nmap <silent> <leader>c a</<C-X><C-O><C-X><Esc>x


""Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
""If you're using tmux version 2.2 or later, you can remove the outermost
""$TMUX check and use tmux's 24-bit color support
""(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more
""information.)
"if (empty($TMUX))
"  if (has("nvim"))
"    "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
"    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
"  endif
"  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 <
"  "https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
"  "Based on Vim patch 7.4.1770 (`guicolors` option) <
"  "https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
"  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
"  if (has("termguicolors"))
"    set termguicolors
"  endif
"endif

" Remap ESC to ii
:imap ii <Esc>
:tmap ii <Esc>
:tmap <S-i><S-i> <C-\><C-n>

" tab navigation mappings
map gn :tabnew

" better backup, swap and undos storage
set directory=~/.vim/dirs/tmp     " directory to place swap files in
set backup                        " make backup files
set backupdir=~/.vim/dirs/backups " where to put backup files
set undofile                      " persistent undos - undo after you re-open the file
set undodir=~/.vim/dirs/undos
set viminfo+=n~/.vim/dirs/viminfo
" store yankring history file there too
let g:yankring_history_dir = '~/.vim/dirs/'

" create needed directories if they don't exist
if !isdirectory(&backupdir)
    call mkdir(&backupdir, "p")
endif
if !isdirectory(&directory)
    call mkdir(&directory, "p")
endif
if !isdirectory(&undodir)
    call mkdir(&undodir, "p")
endif

" toggle nerdtree display
map <F3> :NERDTreeToggle<CR>
" open nerdtree with the current file selected
" nmap <leader>t :NERDTreeFind<CR>
" don;t show these file types
let NERDTreeIgnore = ['\.pyc$', '\.pyo$']


" Lo de los brackets y parentesis
inoremap ( ()<Esc>i
inoremap [ []<Esc>i
inoremap { {}<Esc>i
autocmd Syntax html,vim inoremap < <lt>><Esc>i| inoremap > <c-r>=ClosePair('>')<CR>
inoremap ) <c-r>=ClosePair(')')<CR>
inoremap ] <c-r>=ClosePair(']')<CR>
" inoremap } <c-r>=CloseBracket()<CR>
inoremap } <c-r>=ClosePair('}')<CR>
" inoremap " <c-r>=QuoteDelim('"')<CR>
" inoremap ' <c-r>=QuoteDelim("'")<CR>

function ClosePair(char)
 if getline('.')[col('.') - 1] == a:char
 return "\<Right>"
 else
 return a:char
 endif
endf

function CloseBracket()
 if match(getline(line('.') + 1), '\s*}') < 0
 return "\<CR>}"
 else
 return "\<Esc>j0f}a"
 endif
endf

function QuoteDelim(char)
 let line = getline('.')
 let col = col('.')
 if line[col - 2] == "\\"
 "Inserting a quoted quotation mark into the string
 return a:char
 elseif line[col - 1] == a:char
 "Escaping out of the string
 return "\<Right>"
 else
 "Starting a string
 return a:char.a:char."\<Esc>i"
 endif
endf


" tab length exceptions on some file types
autocmd FileType html setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType php setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType htmldjango setlocal shiftwidth=4 tabstop=4 softtabstop=4
autocmd FileType haskell setlocal shiftwidth=2 tabstop=2 softtabstop=2

let g:pymode_rope = 1
let g:pymode_rope_completion = 1
let g:pymode_indent = 1
let g:pymode_motion = 1
let g:pymode_run = 0
let g:pymode_syntax = 1
let g:pymode_syntax_all = 1
let g:pymode_syntax_print_as_function = 1
let g:pymode_syntax_indent_errors = g:pymode_syntax_all
let g:pymode_syntax_space_errors = g:pymode_syntax_all
let g:pymode_syntax_highlight_exceptions = g:pymode_syntax_all
"let g:pymode_rope_project_root += %:p:h
""let g:pymode_rope_rename_bind = '<C-x><C-x>'
let g:pymode_python = 'python'
" nnoremap <leader>p let g:pymode_python = 'python'
" nnoremap <leader>P let g:pymode_python = 'python3'
" noremap <buffer> <leader>r :exec '!python' shellescape(@%, 1)<cr>
let python_highlight_all = 1

let g:gruvbox_contrast_dark='medium'
let g:gruvbox_contrast_light='medium'
if has('termguicolors')
  set termguicolors
endif

" For dark version.
set background=dark

" For light version.
" set background=light

" Set contrast.
" This configuration option should be placed before `colorscheme gruvbox-material`.
" Available values: 'hard', 'medium'(default), 'soft'
" let g:gruvbox_material_background = 'medium'


nnoremap <silent> [oh :call gruvbox#hls_show()<CR>
nnoremap <silent> ]oh :call gruvbox#hls_hide()<CR>
nnoremap <silent> coh :call gruvbox#hls_toggle()<CR>

nnoremap * :let @/ = ""<CR>:call gruvbox#hls_show()<CR>*
nnoremap / :let @/ = ""<CR>:call gruvbox#hls_show()<CR>/
nnoremap ? :let @/ = ""<CR>:call gruvbox#hls_show()<CR>?


" set tabstop=4
" set shiftwidth=4
set tabstop=4 softtabstop=0 shiftwidth=4 smarttab


" mapping to make movements operate on 1 screen line in wrap mode
let b:gmove = "yes"
function! ScreenMovement(movement)
  if exists("b:gmove") && &wrap && b:gmove == 'yes'
    return "g" . a:movement
  else
    return a:movement
  endif
endfunction
onoremap <silent> <expr> j ScreenMovement("j")
onoremap <silent> <expr> k ScreenMovement("k")
onoremap <silent> <expr> 0 ScreenMovement("0")
onoremap <silent> <expr> ^ ScreenMovement("^")
onoremap <silent> <expr> $ ScreenMovement("$")
nnoremap <silent> <expr> j ScreenMovement("j")
nnoremap <silent> <expr> k ScreenMovement("k")
nnoremap <silent> <expr> 0 ScreenMovement("0")
nnoremap <silent> <expr> ^ ScreenMovement("^")
nnoremap <silent> <expr> $ ScreenMovement("$")
vnoremap <silent> <expr> j ScreenMovement("j")
vnoremap <silent> <expr> k ScreenMovement("k")
vnoremap <silent> <expr> 0 ScreenMovement("0")
vnoremap <silent> <expr> ^ ScreenMovement("^")
vnoremap <silent> <expr> $ ScreenMovement("$")
vnoremap <silent> <expr> j ScreenMovement("j")
" toggle showbreak
function! TYShowBreak()
  if &showbreak == ''
    set showbreak=>
  else
    set showbreak=
  endif
endfunction
function! TYToggleBreakMove()
  if exists("b:gmove") && b:gmove == "yes"
    let b:gmove = "no"
  else
    let b:gmove = "yes"
  endif
endfunction
nmap  <expr> <leader>b  TYShowBreak()
nmap  <expr> <leader>bb  TYToggleBreakMove()


nmap <leader>u :w<CR>:!dos2unix %:p<CR>:edit<CR>
nmap <leader>d :w<CR>:!unix2dos %:p<CR>:edit<CR>

if exists('+colorcolumn')
    set colorcolumn=80
endif

set textwidth=80

" Red after 80th character
" au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)

set tabpagemax=64

au BufNewFile,BufRead *.ms set filetype=groff

if &term =~ '256color'
    " disable Background Color Erase (BCE) so that color schemes
    " render properly when inside 256-color tmux and GNU screen.
    " see also http://sunaku.github.io/vim-256color-bce.html
    set t_ut=
endif

" File browser built-in configs
let g:netrw_banner=0
let g:netrw_browse_split=0
let g:netrw_altv=1
let g:netrw_liststyle=3
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'

" Show commands
set showcmd

" Haskell
let g:haskell_enable_quantification = 1   " to enable highlighting of `forall`
let g:haskell_enable_recursivedo = 1      " to enable highlighting of `mdo` and `rec`
let g:haskell_enable_arrowsyntax = 1      " to enable highlighting of `proc`
let g:haskell_enable_pattern_synonyms = 1 " to enable highlighting of `pattern`
let g:haskell_enable_typeroles = 1        " to enable highlighting of type roles
let g:haskell_enable_static_pointers = 1  " to enable highlighting of `static`
let g:haskell_backpack = 1                " to enable highlighting of backpack keywords
let g:haskell_classic_highlighting = 0    " Haskell classic highlighting

" Reload documents
nnoremap <silent> <leader>r :edit!<CR>

" Markdown preview
map <C-l> :w!<CR>:w!/home/initega/tmp/vim-markdown.md<CR>:!grip %:p --export /home/initega/tmp/vim-markdown.html<CR>:!firefox-bin /home/initega/tmp/vim-markdown.html > /dev/null 2> /dev/null &<CR><CR>


"" Python
" let g:deoplete#enable_at_startup = 1
" let g:python3_host_prog=/path/to/python/executable/
" disable autocompletion, cause we use deoplete for completion
let g:jedi#completions_enabled = 1
" open the go-to function in split, not another buffer
let g:jedi#use_splits_not_buffers = "right"
let g:neomake_python_enabled_makers = ['pep8', 'pylint', 'flake8']


" replace: replace the file, instead of updating buffer (default: 0)
" stdin: send data to stdin of formatter (default: 0)
" env: prepend environment variables to formatter command
let g:neoformat_enabled_python = ['black']
" call neomake#configure#automake('nrwi', 500)
" let g:neomake_open_list = 5
" let g:neoformat_basic_format_trim = 1
" au BufWritePost *.py :Neoformat

hi HighlightedyankRegion cterm=reverse gui=reverse
let g:highlightedyank_highlight_duration = 100
" let g:SimpylFold_docstring_preview = 1

" Enable trimmming of trailing whitespace
" au BufNewFile,BufRead *.py set foldmethod=indent
au BufNewFile,BufRead *.py
    \ set expandtab       |" replace tabs with spaces
    \ set autoindent      |" copy indent when starting a new line
    \ set tabstop=4
    \ set softtabstop=4
    \ set shiftwidth=4


"" LSP
" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.
" set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" position. Coc only does snippet and additional edit on confirm.
" <cr> could be remapped by other vim plugin, try `:verbose imap <CR>`.
if exists('*complete_info')
  inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
else
  inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
endif

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gR <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>lrn <Plug>(coc-rename)

" Formatting selected code.
" xmap <leader>f  <Plug>(coc-format-selected)
" nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>la  <Plug>(coc-codeaction-selected)
nmap <leader>la  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>lac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>lqf  <Plug>(coc-fix-current)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <leader>la  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <leader>le  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <leader>lc  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <leader>lo  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <leader>ls  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <leader>lj  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <leader>lk  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <leader>lp  :<C-u>CocListResume<CR>



set grepprg=rg\ --vimgrep\ --smart-case\ --follow
" let $FZF_DEFAULT_COMMAND = 'ag --hidden --ignore .git -l -g ""'
let $FZF_DEFAULT_COMMAND = 'rg --hidden -l ""'

" Remember cursor position
augroup vimrc-remember-cursor-position
    autocmd!
    autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
augroup END

" Doom Emacs-like keybindings
nnoremap <silent> <leader>: :Commands<CR>
nnoremap <silent> <leader><leader> :GFiles<CR>

nnoremap <silent> <leader>fF :Clap filer ~<CR>
nnoremap <silent> <leader>ff :Clap filer<CR>

function Save()
	let directory = expand('%:h')
	if !isdirectory(&directory)
		call mkdir(&directory, "p")
	endif
	write!
endfunction

nnoremap <silent> <leader>fs :call Save()<CR>
nnoremap <silent> <leader>fr :History<CR>
nnoremap <silent> <leader>fR :Move
nnoremap <silent> <leader>fU :SudoEdit<CR>
nnoremap <silent> <leader>fS :saveas
nnoremap <silent> <leader>fp :e ~/.config/nvim/init.vim<CR>

nnoremap <silent> <leader>sa :Clap grep2<CR>
nnoremap <silent> <leader>sS :BLines<CR>
nnoremap <silent> <leader>ss :Lines<CR>
nnoremap <silent> <leader>sr :History/<CR>

nnoremap <silent> <leader>cr :History:<CR>
nnoremap <silent> <leader>ws :Windows<CR>

nnoremap <silent> <leader>gc :Commits<CR>
nnoremap <silent> <leader>gC :BCommits<CR>

nnoremap <silent> <leader>bb :Buffers<CR>
nnoremap <silent> <leader>bk :bdelete<CR>
nnoremap <silent> <leader>bK :%bd\|e#<CR>
nnoremap <silent> <leader>bn :bnext<CR>
nnoremap <silent> <leader>bp :bprev<CR>
nnoremap <silent> <leader>b0 :bfirst<CR>
nnoremap <silent> <leader>b$ :blast<CR>
" nnoremap <silent> <leader>bL :call BackBuffer()<CR>
" | is <bar> in vimrc
let g:bufs = [1]
function PopBuffer()
	return remove(g:bufs, -1)
endfunction
function BackBuffer()
    let bufNow = @%
	:execute 'buffer' remove(g:bufs, -1)
	call add(g:bufs, bufNow)
endfunction
nnoremap <silent> <leader>bl :b #<CR>

nnoremap <silent> <leader>tk :tabclose<CR>
nnoremap <silent> <leader>tn :tabnext<CR>
nnoremap <silent> <leader>tp :tabprev<CR>
nnoremap <silent> <leader>t0 :tabfirst<CR>
nnoremap <silent> <leader>t$ :tablast<CR>

" " Jump to tab: <Leader>tt
" function TabName(n)
"     let buflist = tabpagebuflist(a:n)
"     let winnr = tabpagewinnr(a:n)
"     return fnamemodify(bufname(buflist[winnr - 1]), ':t')
" endfunction

" function! s:jumpToTab(line)
"     let pair = split(a:line, ' ')
"     let cmd = pair[0].'gt'
"     execute 'normal' cmd
" endfunction

" nnoremap <Leader>tt :call fzf#run({
" \   'source':  reverse(map(range(1, tabpagenr('$')), 'v:val." "." ".TabName(v:val)')),
" \   'sink':    function('<sid>jumpToTab'),
" \   'down':    tabpagenr('$') + 2
" \ })<CR>

nnoremap <silent> <leader>ww <C-W><C-W>
nnoremap <silent> <leader>ws :split<CR>
nnoremap <silent> <leader>wv :vsplit<CR>
nnoremap <silent> <leader>wd :hid<CR>
nnoremap <silent> <leader>wq :hid<CR>
" nnoremap <silent> <leader>wq :q<CR>
nnoremap <silent> <leader>wc :hid<CR>
nnoremap <silent> <leader>wD :only<CR>
nnoremap <silent> <leader>wj <C-W>j
nnoremap <silent> <leader>wk <C-W>k
nnoremap <silent> <leader>wl <C-W>l
nnoremap <silent> <leader>wh <C-W>h
nnoremap <silent> <leader>wt <C-W>t
nnoremap <silent> <leader>wb <C-W>b
nnoremap <silent> <leader>wp <C-W>p
nnoremap <silent> <leader>wn <C-W>n
nnoremap <silent> <leader>wr <C-W>r
nnoremap <silent> <leader>wR <C-W>R
nnoremap <silent> <leader>wJ <C-W>J
nnoremap <silent> <leader>wK <C-W>K
nnoremap <silent> <leader>wL <C-W>L
nnoremap <silent> <leader>wH <C-W>H
nnoremap <silent> <leader>wT <C-W>T
nnoremap <silent> <leader>w= <C-W>=
nnoremap <silent> <leader>w+ <C-W>+
nnoremap <silent> <leader>w< <C-W><
nnoremap <silent> <leader>w> <C-W>>
nnoremap <silent> <leader>w- <C-W>-
nnoremap <silent> <leader>wO :only

nnoremap <silent> <leader>ot :bo sp +te<CR>i
nnoremap <silent> <leader>oT :te<CR>i
nnoremap <silent> <leader>oc :Calendar -first_day=monday<CR>

function! s:append_dir_with_fzf(line)
  call fzf#run(fzf#wrap({
    \ 'options': ['--prompt', a:line.'> '],
    \ 'source': 'git ls-files $(git rev-parse --show-toplevel) | xargs -n 1 dirname | uniq',
    \ 'sink': {line -> feedkeys("\<esc>:".a:line.line, 'n')}}))
  return ''
endfunction

nnoremap <silent> <leader>nn :NERDTreeToggle<CR>
nnoremap <silent> <leader>nN :NERDTreeToggle ~<CR>

nnoremap <silent> <leader>q :q<CR>
nnoremap <silent> <leader>qq :qa<CR>

if executable('fd')
    let g:ctrlp_user_command = 'fd -c never "" %s'
    let g:ctrlp_use_caching = 0
endif

let g:org_agenda_files=['~/Documents/org/agenda.org']

nnoremap Y y$

let g:ctrlp_mruf_case_sensitive = 0


" Vimwiki
function WikiTemplate(name, template, css)
	let wiki = {}
	let wiki.path = '~/Documents/vimwiki/'.a:name
	let wiki.syntax = 'markdown'
	let wiki.ext = '.md'
	let wiki.path_html = wiki.path.'/site_html'
	let wiki.custom_wiki2html = 'vimwiki_markdown'

	let wiki.template_default = a:template
	let wiki.template_ext = '.tpl'
	let wiki.template_path = wiki.path.'/templates/'
	let $VIMWIKI_TEMPLATE_PATH = wiki.path.'/templates/'
	let $VIMWIKI_TEMPLATE_DEFAULT = a:template
	let $VIMWIKI_TEMPLATE_EXT = '.tpl'

	let wiki.css_name = a:css

	return wiki
endfunction

" let wiki_1 = {}
" let wiki_1.path = '~/Documents/vimwiki/'
" let wiki_1.syntax = 'markdown'
" let wiki_1.ext = '.md'
" let wiki_1.template_default = 'default'
" let wiki_1.path_html = wiki_1.path.'site_html'
" let wiki_1.custom_wiki2html = 'vimwiki_markdown'
" let wiki_1.template_ext = '.tpl'


let g:vimwiki_list = [
			\ WikiTemplate('', 'default', 'style.css'),
			\ WikiTemplate('mundu-berria', 'default', 'style.css'),
			\ WikiTemplate('philosophy', 'default', 'style.css'),
			\ {'auto_diary_index': 1}]

let g:vimwiki_map_prefix = '<Leader>e'
" let g:vimwiki_listsyms = ' ○◐●✓'

let g:taskwiki_taskrc_location = '~/.config/taskwarrior/taskrc'
let g:taskwiki_maplocalleader=g:mapleader.'tw'

let g:calendar_first_day = "monday"
let g:calendar_week_number=1

let g:vimwiki_use_calendar = 1
let g:diary_diary=$HOME.'/Documents/vimwiki/diary'
let g:diary_focus_today = 1
let g:diary_keys = { 'goto_next_month': '>', 'goto_prev_month': '<'}
let g:diary_monday = 1
let g:diary_weeknm = 5 " 1
let g:diary_datetime = 'title'
autocmd FileType calendar nmap <buffer> <CR> :<C-u>call vimwiki#diary#calendar_action(b:calendar.day().get_day(), b:calendar.day().get_month(), b:calendar.day().get_year(), b:calendar.day().week(), "V")<CR>

" Hide characters like * or _ get hided and showed when cursor is on top
au BufEnter *.wiki set concealcursor-=n
au BufEnter *.md set concealcursor-=n
au BufEnter * silent! lcd %:p:h
au BufNewFile ~/Documents/vimwiki/diary/*.md :silent 0r !~/.config/nvim/bin/generate-vimwiki-diary-template '%'

" let g:vimwiki_ext2syntax = {'.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
" let g:vimwiki_global_ext = 0

" No more green on Clap
let g:clap_enable_background_shadow = v:false

let g:table_mode_map_prefix='<Leader>tt'
" Table mode always active not working:
" let g:table_mode_always_active = 0
